<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
      //Seed Types
      DB::table('types')->insert(['name' => 'Análisis','status' => 1]);
      DB::table('types')->insert(['name' => 'Condiciones','status' => 1]);
      DB::table('types')->insert(['name' => 'Patrones','status' => 1]);

      //Seed difficulties
      DB::table('difficulties')->insert(['name' => 'Fácil','status' => 1]);
      DB::table('difficulties')->insert(['name' => 'Intermedio','status' => 1]);
      DB::table('difficulties')->insert(['name' => 'Difícil','status' => 1]);

      //Seed Courses
      DB::table('courses')->insert(['name' => 'PorgramAcción I','status' => 1]);


      //Seed Topics
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Bases','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Condicionales','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Ciclos','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Vectores','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Matrices','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Funciones','status' => 1]);
      DB::table('topics')->insert(['course_id' => 1,'name' => 'Archivos','status' => 1]);


      //Seed Topics
      DB::table('roles')->insert(['name' => 'Profesor','status' => 1]);
      DB::table('roles')->insert(['name' => 'Tutor','status' => 1]);
      DB::table('roles')->insert(['name' => 'Estudiante','status' => 1]);
      

    }
}
