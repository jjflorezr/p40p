<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallenge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('challenges', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('type');
          $table->integer('difficulty')->nullable();
          $table->integer('max_score')->nullable();
          $table->integer('owner');
          $table->string('title');
          $table->text('text');
          $table->text('input')->nullable();
          $table->text('output')->nullable();
          $table->integer('score')->nullable();
          $table->integer('status');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('challenges');
    }
}
