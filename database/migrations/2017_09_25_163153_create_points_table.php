<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('points', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('teacher_id')->nullable();
          $table->integer('user_id')->nullable();
          $table->integer('task_id');
          $table->integer('score');
          $table->integer('solution_id')->nullable();
          $table->integer('challenge_id')->nullable();
          $table->text('comment')->nullable();
          $table->integer('status');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('points');
    }
}
