<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Solution extends Model
{
    protected $table = 'solutions';

    public function changeStatusForCahllenge($challenge_id,$user_id)
    {
      Solution::where('challenge_id','=',$challenge_id)
              ->where('user_id','=',$user_id)
              ->update(['challenge_status'=>0]);
    }

    public function countSolutions($level)
    {
    	$count = DB::table('solutions')
    		->join('challenges', function ($join) use ($level) {
                 $join->on('solutions.challenge_id', '=', 'challenges.id')
                      ->where('challenges.difficulty', '=', $level);
             })
            ->where('solutions.challenge_status','=',1)
            ->where('solutions.user_id','=',auth()->user()->id)
            ->count();
         return $count;
    }

}
