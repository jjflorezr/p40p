<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Resource extends Model
{
    protected $table = 'resources';

    public function listActive()
    {
      $resources=Resource::where('status','=',1)
                ->orderBy('created_at','desc')->get();
      return $resources;
    }
}
