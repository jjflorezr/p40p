<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();
Route::get('/','HomeController@getMini');
Route::post('/access','HomeController@access');


Route::group(['middleware' => 'auth'], function() {

	Route::get('/home', 'HomeController@index');

	Route::get('challenges/solve/{id}', 'ChallengeController@solve');
	Route::get('challenges/tryAgain/{id}', 'ChallengeController@tryAgain');
	Route::post('challenges/save', 'ChallengeController@save');
	Route::get('challenges/ready', 'ChallengeController@ready');
	Route::get('challenges/rate/{id}/{usuario}', 'ChallengeController@rate');
	Route::post('challenges/saveRate', 'ChallengeController@saveRate');
	Route::get('challenges/ListForRateFriends/{id}', 'ChallengeController@listForRateFriends');
	Route::get('challenges/rateFriends/{id}/{usuario}', 'ChallengeController@rateFriend');
	Route::post('challenges/saveRateToFriend', 'ChallengeController@saveRateToFriend');
	Route::get('challenges/quality/{id}', 'ChallengeController@quality');
	Route::post('challenges/saveQuality', 'ChallengeController@saveQuality');
	Route::resource('challenges', 'ChallengeController');
	Route::resource('questions', 'QuestionController');
	Route::resource('resources', 'ResourceController');

	Route::resource('feedbacks', 'FeedbackController');

	Route::get('/badges', 'SolutionController@getBadges');

	Route::get('/configure', function () {
		return view('configuration');
	});

	Route::get('/roadmap', function () {
	    return view('roadMap');
	});

});
