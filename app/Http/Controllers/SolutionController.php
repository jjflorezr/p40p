<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Solution;

class SolutionController extends Controller
{
    public function getBadges()
    {
    	$s=new Solution;
    	$data['facil']=($s->countSolutions(1)/20)*100;
    	$data['intermedio']=($s->countSolutions(2)/15)*100;
    	$data['dificil']=($s->countSolutions(3)/10)*100;
    	return view('logros')->with('data',$data);
    }
}
