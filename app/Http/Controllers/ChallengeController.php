<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Type;
use App\Difficulty;
use App\Challenge;
use App\Solution;
use App\Point;

class ChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $c=new Challenge;
        $data['challenges']=$c->listChallenges();
        return view('home')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type=new Type;
        $data['types']=$type->listTypes()->get();

        $dif=new Difficulty;
        $data['dif']=$dif->listDifficulties()->get();

        return view('challenge.create')->with('data',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $challenge=new Challenge;
        $challenge->type=$data['type'];
        $challenge->difficulty=$data['difficulty'];
        $challenge->type=$data['type'];
        $challenge->max_score=$data['max_score'];
        $challenge->owner=auth()->user()->id;
        $challenge->title=$data['title'];
        $challenge->text=$data['text'];
        $challenge->input=$data['input'];
        $challenge->output=$data['output'];
        $challenge->score=0;
        $challenge->status=1;
        $challenge->save();

        return redirect('/challenges');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $challenge=new Challenge;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function solve($id)
    {
        $challenge=new Challenge;
        $data['challenge']=$challenge->solveChallenge($id);
        return view('challenge.solve')->with('data',$data);
    }

    public function save(Request $request)
    {
      $input=$request->all();

      $sol=new Solution;
      $sol->changeStatusForCahllenge($input['challenge_id'],auth()->user()->id);


      $solution=new Solution;
      $solution->user_id=auth()->user()->id;
      $solution->challenge_id=$input['challenge_id'];
      $solution->text=$input['text'];
      $solution->challenge_status=1;
      $solution->save();

      $point=new Point;
      //teacher_id, es nulo, es una accion de estudiante
      $point->user_id=auth()->user()->id;
      $point->task_id=$input['task_id'];
      $point->score=1;
      $point->solution_id=$solution->id;
      $point->challenge_id=$input['challenge_id'];
      if ($input['task_id']==1)
      {
        $point->comment="He Cargado la solución al reto";
      }
      else
      {
        if ($input['task_id']==2)
        {
          $point->comment="He Modificado la solución al reto";
        }
      }
      
      $point->save();
      return redirect('/challenges');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tryAgain($id)
    {
        $challenge=new Challenge;
        $data['challenge']=$challenge->solveChallenge($id);
        return view('challenge.tryAgain')->with('data',$data);

    }

    public function ready()
    {
      $challenge=new Challenge;
      $data['challenges']=$challenge->listReadyChallenge();
      return view('challenge.listForRate')->with('data',$data);
    }

    public function rate($idChallenge,$idUsuario)
    {
      $challenge=new Challenge;
      $data['challenge']=$challenge->rateChallenge($idChallenge,$idUsuario);
      return view('challenge.rate')->with('data',$data);
    }

    public function saveRate(Request $request)
    {
      $input=$request->all();

      $point=new Point;
      $point->teacher_id=auth()->user()->id;
      //User_id esta inmerso en la solución
      $point->task_id=$input['task_id'];
      $point->score=$input['score'];
      $point->solution_id=$input['solution_id'];
      $point->challenge_id=$input['challenge_id'];
      if(trim($input['comment'])<>"")
      {
        $point->comment=trim($input['comment']);
      }
      else {
        $point->comment="El profesor a calificado la solucion al reto";
      }

      $point->save();
      return redirect('/challenges');
    }


    public function rateFriend($idChallenge,$idUsuario)
    {
      $challenge=new Challenge;
      $data['challenge']=$challenge->rateChallenge($idChallenge,$idUsuario);
      return view('challenge.rateFriend')->with('data',$data);
    }

    public function saveRateToFriend(Request $request)
    {
      $input=$request->all();

      $point=new Point;
      $point->teacher_id=auth()->user()->id;
      //User_id esta inmerso en la solución
      $point->task_id=$input['task_id'];
      $point->score=$input['score'];
      $point->solution_id=$input['solution_id'];
      $point->challenge_id=$input['challenge_id'];
      if(trim($input['comment'])<>"")
      {
        $point->comment=trim($input['comment']);
      }
      else {
        $point->comment="Un compañero a calificado la solución al reto";
      }

      $point->save();

      $point=new Point;
      $point->user_id=auth()->user()->id;
      $point->task_id=6;
      $point->solution_id=$input['solution_id'];
      $point->challenge_id=$input['challenge_id'];
      $point->comment="He calificado un reto de un compañero";
      $point->save();
      return redirect('/challenges');
    }

    public function listForRateFriends($idChallenge)
    {
      $challenge=new Challenge;
      $data['challenges']=$challenge->listFriendsChallenges($idChallenge);
      return view('challenge.listForRateFriends')->with('data',$data);
    }

    public function quality($id)
    {
        $challenge=new Challenge;
        $data['challenge']=$challenge->solveChallenge($id);
        return view('challenge.quality')->with('data',$data);
    }

    public function saveQuality(Request $request)
    {
      $input=$request->all();

      $point=new Point;
      //teacher_id, es nulo, es una accion de estudiante
      $point->user_id=auth()->user()->id;
      $point->task_id=$input['task_id'];
      $point->score=$input['score'];
      $point->solution_id=$input['solution_id'];
      $point->challenge_id=$input['challenge_id'];
      if(trim($input['comment'])<>"")
      {
        $point->comment=trim($input['comment']);
      }
      else {
        $point->comment="He calificado la calidad de un reto";
      }

      $point->save();
      return redirect('/challenges');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
