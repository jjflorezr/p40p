<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Challenge;
use App\Mini;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $c=new Challenge;
        $data['challenges']=$c->listChallenges();
        return view('home')->with('data',$data);
    }

    public function getMini()
    {
        $m=new Mini;
        $data['mini']=$m->getMini();
        return view('index')->with('data',$data);
    }

    public function access(Request $request)
    {
        $data=$request->all();
        $m=new Mini;
        $validate=$m->validateAnswer($data['answer']);

        if($validate==0)
        {
            return redirect('/');
        }
        else
        {
            return redirect('/home');
        }
    }
}
