<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    public function listQuestions()
    {
      $questions=Question::where('status','=',1)
                ->orderBy('created_at','desc')->get();
      return $questions;
    }

}
