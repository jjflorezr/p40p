<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    public function listFeedbacks()
    {
      $feedbacks=Feedback::where('status','=',1)
                ->orderBy('created_at','desc')->get();
      return $feedbacks;
    }
}
