<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mini extends Model
{
    protected $table = 'minis';

    public function getMini()
    {
      $questions=Mini::where('status','=',1)->get();
      return $questions;
    }

    public function validateAnswer($answer)
    {
    	$count=Mini::where('status','=',1)
    			->where('answer','=',$answer)
    			->count();
    	return $count;
    }
}
