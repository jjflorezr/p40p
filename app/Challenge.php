<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Challenge extends Model
{
    protected $table = 'challenges';

    public function listChallenges()
    {
      $challenges = DB::table('challenges')
            ->join('types', 'challenges.type', '=', 'types.id')
            ->join('difficulties', 'challenges.difficulty', '=', 'difficulties.id')
            ->join('users', 'challenges.owner', '=', 'users.id')
            ->leftJoin('solutions', function ($join) {
                 $join->on('challenges.id', '=', 'solutions.challenge_id')
                      ->where('solutions.user_id', '=', auth()->user()->id)
                      ->where('solutions.challenge_status', '=', 1);
             })
            ->select('challenges.*', 'types.name as tipo', 'difficulties.name as dificultad','solutions.challenge_status','users.name as usuario')
            ->get();
      return $challenges;
    }

    public function solveChallenge($id)
    {
      $challenge = DB::table('challenges')
            ->join('types', 'challenges.type', '=', 'types.id')
            ->join('difficulties', 'challenges.difficulty', '=', 'difficulties.id')
            ->join('users', 'challenges.owner', '=', 'users.id')
            ->leftJoin('solutions', function ($join) {
                 $join->on('challenges.id', '=', 'solutions.challenge_id')
                      ->where('solutions.user_id', '=', auth()->user()->id)
                      ->where('solutions.challenge_status', '=', 1);
             })
            ->where('challenges.id','=',$id)
            ->select('challenges.*', 'types.name as tipo', 'difficulties.name as dificultad','solutions.challenge_status','solutions.text as solution','solutions.id as idSolucion')
            ->get();
      return $challenge;
    }

    public function listReadyChallenge()
    {
      $challenges = DB::table('solutions')
            ->join('challenges', 'solutions.challenge_id', '=', 'challenges.id')
            ->join('users', 'solutions.user_id', '=', 'users.id')
            ->where('solutions.challenge_status','=',1)
            ->select('challenges.*','solutions.challenge_status','users.name as usuario','users.id as idUsuario')
            ->get();
      return $challenges;
    }

    public function rateChallenge($idChallenge,$idUsuario)
    {

      $challenge = DB::table('challenges')
            ->join('types', 'challenges.type', '=', 'types.id')
            ->join('difficulties', 'challenges.difficulty', '=', 'difficulties.id')
            ->join('users', 'challenges.owner', '=', 'users.id')
            ->leftJoin('solutions', function ($join) use($idUsuario) {
                 $join->on('challenges.id', '=', 'solutions.challenge_id')
                      ->where('solutions.user_id', '=', $idUsuario)
                      ->where('solutions.challenge_status', '=', 1);
             })
            ->where('challenges.id','=',$idChallenge)
            ->select('challenges.*','solutions.challenge_status','solutions.text as solution','solutions.id as idSolucion')
            ->get();
      return $challenge;
    }

    public function listFriendsChallenges($challenge_id)
    {
      $challenges = DB::table('solutions')
            ->join('challenges', 'solutions.challenge_id', '=', 'challenges.id')
            ->join('users', 'solutions.user_id', '=', 'users.id')
            ->where('solutions.challenge_status','=',1)
            ->where('solutions.user_id','<>',auth()->user()->id)
            ->where('solutions.challenge_id','=',$challenge_id)
            ->select('challenges.*','solutions.challenge_status','users.name as usuario','users.id as idUsuario')
            ->get();
      return $challenges;
    }

    public function rateFriends($idChallenge)
    {

      $challenge = DB::table('challenges')
            ->join('types', 'challenges.type', '=', 'types.id')
            ->join('difficulties', 'challenges.difficulty', '=', 'difficulties.id')
            ->join('users', 'challenges.owner', '=', 'users.id')
            ->leftJoin('solutions', function ($join){
                 $join->on('challenges.id', '=', 'solutions.challenge_id')
                      ->where('solutions.user_id', '<>', auth()->user()->id)
                      ->where('solutions.challenge_status', '=', 1);
             })
            ->where('challenges.id','=',$idChallenge)
            ->select('challenges.*','solutions.challenge_status','solutions.text as solution','solutions.id as idSolucion')
            ->get();
      return $challenge;
    }

}
