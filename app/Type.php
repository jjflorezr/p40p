<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
  //Base de datos usada por el modelo
	protected $table = 'types';
	//Campos de la tabla
	//protected $fillable = ['f728_nombre','f728_descripcion','f728_id_tipo_accion','f728_estado'];
	//Campos ocultos
	//protected $hidden = [];
	//protected $primaryKey = 'f728_id';

public function listTypes()
{
  $types=Type::where('status','=',1);
  return $types;
}


}
