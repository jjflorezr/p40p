<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>P40P</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=BenchNine:300|Bungee|Share+Tech+Mono" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
            background-color: #2c2c2c;
            margin: 0;
        }
        .divRed
        {
          background-color: #E21E32;
          width: 100%;
          padding: 5px;
        }

        .title
        {
          font-family: 'Bungee', cursive;
          font-size: 26pt;
          color: #FFFFFF;
        }

        .fa-btn {
            margin-right: 6px;
        }


        .principalTitle
        {
          font-family: 'Bungee', cursive;
          font-size: 26pt;
          color: #000000;
        }

        .bigGreen {
          background-color: #F1FAEE;
          font-family: 'Share Tech Mono', monospace;
          font-size: 16pt;
          color: #1B1B1E;
        }


        .bigBlack {
          background-color: #5BC0BE;
          font-family: 'Share Tech Mono', monospace;
          font-size: 16pt;
          color: #FFFFFF;
        }

        .iconYellow
        {
          color: #F0C808;
        }

        .iconRed
        {
          color: #DD1C1A;
        }

        .iconDarkBlue
        {
          color: #00171F;
        }

        .iconOrange
        {
          color: #FA7921;
        }

        .navbar
        {
          background-color: #2c2c2c;
          border-color: #2c2c2c;
        }

        .tituloReto
        {
          font-family: 'Open Sans', sans-serif;
          color:#E21E32;
          font-size: 20pt;
        }

        .fondoTitulo
        {
          background-color: #f4d249;
          color: #2c2c2c;
          font-family: 'Open Sans', sans-serif;
          font-size: 20pt;
        }

        .boton
        {
          font-family: 'Open Sans', sans-serif;
          font-size: 16pt;
          padding: 10px;
          background-color: #E21E32;
          color:  #FFFFFF;
          border:  #E21E32 1px solid;
        }

        a:link
        {
         text-decoration:none;
         color:  #FFFFFF;
        }

        a:hover{
          text-decoration:none;
          color:  #FFFFFF;
        }

        .textGreen
        {
          color:  #00A85C;
        }

        .sinBorde
        {
          border:  0;
        }


    </style>
</head>
<body id="app-layout">
  <div class="divRed">
  </div>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand principalTitle" href="{{ url('/home') }}">
                    <p class="title">P40P</p>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                  @if (!Auth::guest())
                    <!--<li><a href="{{ url('/configure') }}">Configuración</a></li>-->
                    <li><a href="{{ url('/badges') }}">Mis Logros</a></li>
                    <li><a href="{{ url('/questions/create') }}">Preguntar</a></li>
                    <li><a href="{{ url('/feedbacks/create') }}">Feedback</a></li>
                    <li><a href="{{ url('/resources') }}">Recursos</a></li>
                    <li><a href="{{ url('/roadmap') }}">¡Avances!</a></li>
                  @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Registro</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
