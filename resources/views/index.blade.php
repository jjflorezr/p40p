<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>P40P</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=BenchNine:300|Bungee|Share+Tech+Mono" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <style media="screen">
      body{
        background-color: #2c2c2c;
        margin: 0;
      }
      .divRed
      {
        background-color: #E21E32;
        width: 100%;
        padding: 5px;
      }
      .title
      {
        font-family: 'Bungee', cursive;
        font-size: 26pt;
        color: #FFFFFF;
      }
      .question
      {
        margin-top: 15%;
        font-family: 'Open Sans', sans-serif;
        font-size: 56pt;
        color: #FFFFFF;
      }
      .answer
      {
        font-family: 'Open Sans', sans-serif;
        font-size: 56pt;
        background-color: #2c2c2c;
        border: 0;
        border-bottom: #E21E32 solid 2px;
        width:500px;
        color:#F4D249;
        text-align: center;
      }

     input:focus{
        outline:0px;
      }

      .boton
      {
        font-family: 'Open Sans', sans-serif;
        font-size: 16pt;
        padding: 10px;
        background-color: #2c2c2c;
        color:  #E21E32;
        border:  #E21E32 1px solid;
      }

    </style>
  </head>
  <body>
    <div class="divRed">
    </div>
    <div class="row">
      <div class="col-md-11 col-md-offset-1">
        <p class="title">P40P</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <p class="question">{{$data['mini'][0]->question}}</p>
        <br>
        <form class="form-horizontal" method="POST" action="{{url('/access')}}" id="form">
          {{ csrf_field() }}
          <input type="text" name="answer" value="" class="answer">
          <br><br><br>
          <button type="submit" class="boton">Acceder</button>
          </div>
        </form>
      </div>
    </div>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </body>
</html>
