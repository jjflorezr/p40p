@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel sinBorde">
                <div class="panel-heading fondoTitulo">Nuevo Reto</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ url('/challenges') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="tipo" class="col-sm-2 control-label">Tipo:</label>
                      <div class="col-sm-10">
                        <select class="form-control input-lg" name="type" required>
                          <option value="">Tipo de Reto</option>
                          @foreach($data['types'] as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="dificultad" class="col-sm-2 control-label">Dificultad:</label>
                      <div class="col-sm-10">
                        <select class="form-control input-lg" name="difficulty" required>
                          <option value="">Dificultad</option>
                          @foreach($data['dif'] as $dif)
                            <option value="{{$dif->id}}">{{$dif->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="max_score" class="col-sm-2 control-label">Puntaje Maximo:</label>
                      <div class="col-sm-10">
                        <input class="form-control input-lg" type="text" placeholder="" name="max_score" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="title" class="col-sm-2 control-label">Titulo:</label>
                      <div class="col-sm-10">
                        <input class="form-control input-lg" type="text" placeholder="" name="title" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="text" class="col-sm-2 control-label">Reto:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" rows="10" name="text" required></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="input" class="col-sm-2 control-label">Entrada:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="input" required></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="output" class="col-sm-2 control-label">Salida:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="output" required></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-info">Guardar</button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
