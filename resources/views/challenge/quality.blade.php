@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-md-10 col-md-offset-1">
          <div class="panel sinBorde">
              <div class="panel-heading fondoTitulo"><h3>¿Califica la Calidad del Enunciado?</h3></div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12 text-justify">
                    {{$data['challenge'][0]->text}}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Entradas</h3>
                    <samp>{!!nl2br(e($data['challenge'][0]->input))!!}</samp>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Salidas</h3>
                    <samp>{!!nl2br(e($data['challenge'][0]->output))!!}</samp>

                  </div>
                </div>
                <h3>Mi Solución</h3>
                <samp>{!!nl2br(e($data['challenge'][0]->solution))!!}</samp>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                      <form class="form-horizontal" method="POST" action="{{ url('/challenges/saveQuality') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="challenge_id" value="{{$data['challenge'][0]->id}}">
                        <input type="hidden" name="task_id" value="3">
                        <input type="hidden" name="solution_id" value="{{$data['challenge'][0]->idSolucion}}">
                        <div class="form-group">
                          <div class="col-sm-10 col-md-offset-1">
                            <select class="form-control input-lg" name="score" required>
                              <option value="">Calificación</option>
                              <option value="1">Bueno</option>
                              <option value="2">Regular</option>
                              <option value="3">Malo</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-10 col-md-offset-1">
                            <textarea class="form-control" rows="4" name="comment" placeholder="Comentario" required></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-1 col-sm-10">
                            <button type="submit" class="btn btn-danger">Calificar</button>
                          </div>
                        </div>
                      </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection
