@extends('layouts.app')
@section('content')
<div class="container">
      <div class="col-md-10 col-md-offset-1">
        @foreach($data['challenges'] as $challenge)
          <div class="panel panel-default">
            <div class="panel-body bigGreen">
              <div class="row">
                <div class="col-md-12">
                  @if($challenge->challenge_status<>null)
                    <p class="tituloReto">{{$challenge->title}}</p>
                  @else
                    <p class="tituloReto">{{$challenge->title}}</p>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-8 text-justify">
                  ------ {{$challenge->usuario}}
                </div>
                <div class="col-md-4 text-right">
                  <p>
                    <a href="{{ url('/challenges/rateFriends/'.$challenge->id.'/'.$challenge->idUsuario) }}" class="boton">Calificar</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
</div>
@endsection
