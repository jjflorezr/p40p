@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-md-10 col-md-offset-1">
          <div class="panel sinBorde">
              <div class="panel-heading fondoTitulo"><h3>{{$data['challenge'][0]->title}}</h3></div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12 text-justify">
                    {{$data['challenge'][0]->text}}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Entradas</h3>
                    <samp>{!!nl2br(e($data['challenge'][0]->input))!!}</samp>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Salidas</h3>
                    <samp>{!!nl2br(e($data['challenge'][0]->output))!!}</samp>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                      <form class="form-horizontal" method="POST" action="{{ url('/challenges/save') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="challenge_id" value="{{$data['challenge'][0]->id}}">
                        <input type="hidden" name="task_id" value="2">
                        <div class="form-group">
                          <div class="col-sm-10 col-md-offset-1">
                            <textarea class="form-control" rows="10" name="text">
                              {{trim($data['challenge'][0]->solution)}}
                            </textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-1 col-sm-10">
                            <button type="submit" class="btn btn-info">Resolver</button>
                          </div>
                        </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection
