@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel sinBorde">
                <div class="panel-heading fondoTitulo">Mis Logros</div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-sm-6 col-md-3">
                       <div class="thumbnail">
                         <img src="{{url('img/coin.png')}}">
                         <div class="caption">
                           <h3>Puntos</h3>
                           <p>Puntos Generales por participación</p>
                           <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                              <span class="sr-only">0% Complete (success)</span>
                            </div>
                          </div>
                         </div>
                       </div>
                     </div>
                     <div class="col-sm-6 col-md-3">
                      <div class="thumbnail">
                        <img src="{{url('img/ojo.png')}}">
                        <div class="caption">
                          <h3>Calificación</h3>
                          <p>Calificación de retos propuestos</p>
                          <div class="progress">
                           <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                             <span class="sr-only">40% Complete (success)</span>
                           </div>
                         </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                     <div class="thumbnail">
                       <img src="{{url('img/easy.png')}}">
                       <div class="caption">
                         <h3>Fáciles</h3>
                         <p>Solución de retos faciles propuestos</p>
                         <div class="progress">
                          <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$data['facil']}}%">
                            <span class="sr-only">{{$data['facil']}}% Complete (success)</span>
                          </div>
                        </div>
                       </div>
                     </div>
                   </div>
                   <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                      <img src="{{url('img/superman.jpg')}}">
                      <div class="caption">
                        <h3>Intermedios</h3>
                        <p>Solución de retos intermedios propuestos</p>
                        <div class="progress">
                         <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$data['intermedio']}}%">
                           <span class="sr-only">{{$data['intermedio']}}% Complete (success)</span>
                         </div>
                       </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-3">
                   <div class="thumbnail">
                     <img src="{{url('img/mascara.png')}}">
                     <div class="caption">
                       <h3>Difíciles</h3>
                       <p>Solución de retos dificiles propuestos</p>
                       <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$data['dificil']}}%">
                          <span class="sr-only">{{$data['dificil']}}% Complete (success)</span>
                        </div>
                      </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-sm-6 col-md-3">
                  <div class="thumbnail">
                    <img src="{{url('img/echicero.jpeg')}}">
                    <div class="caption">
                      <h3>Creador</h3>
                      <p>Creación y propuesta de retos</p>
                      <div class="progress">
                       <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                         <span class="sr-only">40% Complete (success)</span>
                       </div>
                     </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-3">
                 <div class="thumbnail">
                   <img src="{{url('img/estrella.png')}}">
                   <div class="caption">
                     <h3>Calificador</h3>
                     <p>Calificación de retos de compañeros</p>
                     <div class="progress">
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span class="sr-only">40% Complete (success)</span>
                      </div>
                    </div>
                   </div>
                 </div>
               </div>
               <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                  <img src="{{url('img/calificar.png')}}">
                  <div class="caption">
                    <h3>Preguntón</h3>
                    <p>Preguntas realizadas al grupo</p>
                    <div class="progress">
                     <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                       <span class="sr-only">40% Complete (success)</span>
                     </div>
                   </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-3">
               <div class="thumbnail">
                 <img src="{{url('img/hippie.png')}}">
                 <div class="caption">
                   <h3>Lógico</h3>
                   <p>Solución de retos logicos</p>
                   <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                      <span class="sr-only">40% Complete (success)</span>
                    </div>
                  </div>
                 </div>
               </div>
             </div>
                   </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
