@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-10 col-md-offset-1">
          <div class="row">
          <div class="col-md-12 text-center tituloReto">
            <a href="{{url('/challenges/create')}}"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
          </div>
        </div>
        <br>
        @foreach($data['challenges'] as $challenge)
          <div class="panel panel-default">
            <div class="panel-body bigGreen">
              <div class="row">
                <div class="col-md-12">
                  @if($challenge->challenge_status<>null)
                    <p class="tituloReto"><i class="fa fa-check-circle textGreen" aria-hidden="true"></i> {{$challenge->title}}</p>
                  @else
                    <p class="tituloReto">{{$challenge->title}}</p>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-8 text-justify">
                  <i class="fa fa-star iconYellow" aria-hidden="true"></i> {{$challenge->score}}  <i class="fa fa-rocket iconRed" aria-hidden="true"></i> {{$challenge->max_score}}  <i class="fa fa-connectdevelop iconDarkBlue" aria-hidden="true"></i> {{$challenge->dificultad}} <i class="fa fa-bolt iconOrange" aria-hidden="true"></i> {{$challenge->tipo}}
                </div>
                <div class="col-md-4 text-right">
                  <p>
                    @if($challenge->challenge_status<>null)
                    <a href="{{ url('/challenges/solve/'.$challenge->id) }}" class="boton">ver</a>
                    @else
                      <a href="{{ url('/challenges/solve/'.$challenge->id) }}" class="boton">Resolver</a>
                    @endif
                  </p>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
</div>
@endsection
