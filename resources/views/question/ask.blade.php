@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-10 col-md-offset-1">
      <div class="panel sinBorde">
        <div class="panel-heading fondoTitulo"><h3>Preguntar</h3></div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ url('/questions/') }}">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-sm-10 col-sm-offset-1">
                <textarea class="form-control" rows="10" name="text" placeholder="Escribe tu Pregunta" required></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-10 col-sm-offset-1">
                <select class="form-control input-lg" name="topic_id" required>
                  <option value="">Tema:</option>
                    <option value="1">Bases</option>
                    <option value="2">Condicionales</option>
                    <option value="3">Mientras</option>
                    <option value="4">Para</option>
                    <option value="5">Vectores</option>
                    <option value="6">Matrices</option>
                    <option value="7">Archivos</option>
                    <option value="8">PseInt</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-1 col-sm-10">
                <button type="submit" class="btn btn-info">Preguntar</button>
              </div>
            </div>

          </form>
          <hr>
          <ul class="media-list">
            @foreach($data['questions'] as $q)
              <li class="media">
                <div class="media-left">
                  <a href="#">
                    <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="{{url ('/img/question.png')}}">
                  </a>
                </div>
                <div class="media-body">
                      <h4 class="media-heading">{{$q->created_at}}</h4>
                      <p>{{$q->text}}</p>
                </div>
                </li>
              @endforeach
            </ul>
        </div>
      </div>
  </div>
</div>
@endsection
