@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-4">
       <div class="thumbnail">
         <img src="{{url('img/reto.png')}}">
         <div class="caption">
           <h3>Retos</h3>
           <p>Administración y Gestión de retos</p>
           <p><a href="{{ url('/challenges/create') }}" class="btn btn-success" role="button">Crear</a> <a href="{{ url('/challenges') }}" class="btn btn-info" role="button">Listar</a></p>
         </div>
       </div>
     </div>
     <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img src="{{url('img/task.png')}}">
        <div class="caption">
          <h3>Tareas</h3>
          <p>Administración y Gestión de Tareas</p>
          <p><a href="{{ url('/challenges/create') }}" class="btn btn-success" role="button">Crear</a> <a href="{{ url('/challenges') }}" class="btn btn-info" role="button">Listar</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
     <div class="thumbnail">
       <img src="{{url('img/barbaro.png')}}">
       <div class="caption">
         <h3>Calificar</h3>
         <p>Calificar Retos de Estudiantes</p>
         <p><a href="{{url('/challenges/ready')}}" class="btn btn-danger" role="button">Calificar</a></p>
       </div>
     </div>
   </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="{{url('img/caja.png')}}">
      <div class="caption">
        <h3>Retos de Inicio</h3>
        <p>Gestion de retos inciales</p>
        <p><a href="{{ url('/challenges/create') }}" class="btn btn-success" role="button">Crear</a> <a href="{{ url('/challenges') }}" class="btn btn-info" role="button">Listar</a></p>
      </div>
    </div>
  </div>
    </div>
</div>
@endsection
