@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-10 col-md-offset-1">
      <div class="panel sinBorde">
        <div class="panel-heading fondoTitulo"><h3>Mapa de Ruta</h3></div>
        <div class="panel-body">
        <h3><b>Beta 0.1.1.</b></h3>
        <hr>
          <ol>
            <li><span class="label label-success">Funcionalidad</span>  26.10.17 - Carga de material de clase</li>
            <li><span class="label label-success">Funcionalidad</span>  26.10.17 - Visualización de material de clase</li>
            <li><span class="label label-success">Funcionalidad</span>  9.10.17 - Visualización avance de logro retos Dificiles</li>
            <li><span class="label label-success">Funcionalidad</span>  9.10.17 - Visualización avance de logro retos Intermedios</li>
            <li><span class="label label-success">Funcionalidad</span>  9.10.17 - Visualización avance de logro retos Faciles</li>
            <li><span class="label label-danger">bug</span>  7.10.17 - Dirección a login al cierre de sesión</li>
            <li><span class="label label-danger">bug</span>  6.10.17 - Control de campos obligatorios al calificar reto</li>
            <li><span class="label label-danger">bug</span>  6.10.17 - Control de campos obligatorios al resolver reto</li>
            <li><span class="label label-success">Funcionalidad</span>  6.10.17 - Escribir y leer feedback</li>
            <li><span class="label label-danger">bug</span>  6.10.17 - Control de campos obligatorios al hacer pregunta</li>
            <li><span class="label label-danger">bug</span>  6.10.17 - Control de campos obligatorios al crear reto</li>
            <li><span class="label label-danger">bug</span>  5.10.17 - Corrección en la descripción de logros</li>
            <li><span class="label label-danger">bug</span>  5.10.17 - Redirección a retos despues de ejecutar acción</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Cargar retos</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Listar retos</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Solucionar retos</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Modificar solución de retos</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Calificar el reto de compañeros</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Calificar claridad y calidad de los retos propuestos</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Presentación de mini reto de ingreso</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Calificación de retos por parte de tutores</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Preguntar al grupo</li>
            <li><span class="label label-success">Funcionalidad</span>  5.10.17 - Consolidado de logros</li>
          </ol>
        </div>
      </div>
  </div>
</div>
@endsection
