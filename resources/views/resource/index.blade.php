@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-10 col-md-offset-1">
      <div class="panel sinBorde">
        <div class="panel-heading fondoTitulo"><h3>Recursos de Curso</h3></div>
        <div class="panel-body bigGreen">
          <ul>
          @foreach($data['resources'] as $r)
              <div class="row">
                <div class="col-md-10">
                  {{$r->name}}
                </div>
                <div class="col-md-2">
                  <a href="{{url('/doc/'.$r->path)}}" type="button" class="btn btn-success btn-xs">Descargar</a>
                </div>
              </div>
          @endforeach
          </ul>
        </div>
      </div>
  </div>
</div>
@endsection
