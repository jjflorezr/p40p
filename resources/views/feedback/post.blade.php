@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-10 col-md-offset-1">
      <div class="panel sinBorde">
        <div class="panel-heading fondoTitulo"><h3>Escribe tu Opinión ó Sugerencia</h3></div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ url('/feedbacks') }}">
          {{ csrf_field() }}
            <div class="form-group">
              <div class="col-sm-10 col-sm-offset-1">
                <textarea class="form-control" rows="10" name="text" placeholder="Escribe tu Opinión o Sugerencia" required></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-1 col-sm-10">
                <button type="submit" class="btn btn-danger">Guardar</button>
              </div>
            </div>
          </form>
          <hr>
            <ul class="media-list">
              @foreach($data['feedbacks'] as $f)
                <li class="media">
                  <div class="media-left">
                    <a href="#">
                      <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="{{url ('/img/feedback.png')}}">
                    </a>
                  </div>
                  <div class="media-body">
                      <h4 class="media-heading">{{$f->created_at}}</h4>
                      <p>{{$f->text}}</p>
                  </div>
                </li>
              @endforeach
            </ul>
        </div>
      </div>
  </div>
</div>
@endsection
